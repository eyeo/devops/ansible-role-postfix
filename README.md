# ansible-role-postfix

This is an [Ansible](https://docs.ansible.com/ansible/latest/) role for
compiling and deploying the [postfix](http://www.postfix.org/documentation.html)
software. 

This role is named postfix/agent because it is used to listen on localhost
for local system services and send outgoing mails.

## Synopsis

Below please find an example on how to include the role and configure the
individual postfix agent instances in an Ansible Playbook:

    - hosts:
        "postfix-servers"

      tasks:
        - import_role:
            name: "agent"

      vars:
        postfix_configuration:
          inet_protocols: "all"
          inet_interfaces: "127.0.0.1 [::1]"

## Variables

All role variables are documented in the [`agent/defaults/main.yml`](./agent/defaults/main.yml) file.
